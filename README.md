# Teste de Código Python/Django

## 1 - Instruções para executar o projeto localmente com Docker:

- No terminal executar utilizar o comando:

```
docker-compose up --build
```

- Após finalizar com sucesso o build do projeto, basta utilizar o endereço abaixo para testes:

```
http://localhost:8000/
```


## Instruções para executar os testes localmente:

- Executar o Postgres do docker-compose.yaml digitando o seguinte comando no terminal:

```
docker-compose up -d db
```

- Instalar a biblioteca Poetry:

```
pip install poetry
```

- Criar o ambiente virtual com o comando:

```
poetry shell
```

- Instalar as dependências com o comando:

```
poetry install
```

- Executar os testes com o comando:

```
pytest -v app_freelancers/api/tests/*
```
