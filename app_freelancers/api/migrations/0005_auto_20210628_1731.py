# Generated by Django 3.2.4 on 2021-06-28 17:31

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20210628_1728'),
    ]

    operations = [
        migrations.RenameField(
            model_name='professionalexperience',
            old_name='professional_profile',
            new_name='freelancer_profile',
        ),
        migrations.AlterField(
            model_name='freelancerprofile',
            name='query_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 28, 17, 31, 4, 138526)),
        ),
    ]
