# Generated by Django 3.2.4 on 2021-06-28 20:35

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_alter_freelancerprofile_query_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='freelancerprofile',
            name='query_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 28, 20, 35, 8, 849241)),
        ),
        migrations.AlterField(
            model_name='skill',
            name='professional_experience',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='experience_skills', to='api.professionalexperience'),
        ),
    ]
