from api.models import User, UserType


class Company(User):
    base_type = UserType.COMPANY

    class Meta:
        proxy = True
