from django.db import models


class FreelancerProfile(models.Model):
    external_id = models.IntegerField()
    data = models.JSONField()
    query_date = models.DateTimeField()
