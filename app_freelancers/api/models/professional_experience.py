from django.db import models


class ProfessionalExperience(models.Model):
    freelancer_profile = models.ForeignKey(
        'api.FreelancerProfile',
        on_delete=models.CASCADE,
        related_name='experience'
    )
    company_name = models.CharField(max_length=100)
    start_date = models.DateTimeField(null=False)
    end_date = models.DateTimeField(null=False)
