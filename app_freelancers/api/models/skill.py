from django.db import models
from django.db.models import Min, Max
from django.db.models.functions import ExtractMonth, ExtractYear


class SkillManager(models.Manager):
    def get_total_experience(self, profile_id):
        return Skill.objects.filter(
            professional_experience__freelancer_profile__external_id=profile_id
        ).values(
            'external_id',
            'name'
        ).annotate(
            min_date=Min('professional_experience__start_date'),
            max_date=Max('professional_experience__end_date'),
            total_months=(ExtractYear('max_date') - ExtractYear('min_date')) * 12 +
                         (ExtractMonth('max_date') - ExtractMonth('min_date'))
        )


class Skill(models.Model):
    professional_experience = models.ForeignKey(
        'api.ProfessionalExperience',
        on_delete=models.CASCADE,
        related_name='experience_skills'
    )
    external_id = models.IntegerField()
    name = models.CharField(max_length=100)

    objects = SkillManager()
