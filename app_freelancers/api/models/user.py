from enum import Enum

from django.contrib.auth.models import AbstractUser
from django.db import models


class UserType(Enum):
    COMPANY = 'company'


class User(AbstractUser):
    base_type = UserType.COMPANY

    user_type = models.CharField(
        max_length=50,
        choices=[(tag, tag.value) for tag in UserType],
        default=base_type
    )

    def save(self, *args, **kwargs):
        if not self.id:
            self.user_type = self.base_type

        return super().save(*args, **kwargs)
