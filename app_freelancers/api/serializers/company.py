from api.serializers.user import UserSerializer
from api.models import Company


class CompanySerializer(UserSerializer):

    class Meta:
        model = Company
        fields = ['username', 'password', 'email']
