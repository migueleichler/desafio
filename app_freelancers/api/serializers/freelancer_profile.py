import json
from datetime import datetime

from rest_framework import serializers
from django.core.serializers.json import DjangoJSONEncoder

from api.serializers.professional_experience import ProfessionalExperienceSerializer
from api.models import FreelancerProfile, ProfessionalExperience, Skill


class FreelancerProfileSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    professional_experiences = ProfessionalExperienceSerializer(many=True)

    def to_internal_value(self, data):
        resource_data = data['freelancer'] if 'freelancer' in data else data

        return super().to_internal_value(resource_data)

    def to_representation(self, instance):
        return {'freelancer': instance}

    def create(self, validated_data):
        freelancer_profile = FreelancerProfile.objects.create(
            external_id=validated_data['id'],
            data=json.dumps(validated_data, indent=1, cls=DjangoJSONEncoder),
            query_date=datetime.now()
        )

        for experience_data in validated_data['professional_experiences']:
            professional_experience = ProfessionalExperience.objects.create(
                freelancer_profile=freelancer_profile,
                company_name=experience_data['company_name'],
                start_date=experience_data['start_date'],
                end_date=experience_data['end_date'],
            )

            for skill_data in experience_data['skills']:
                Skill.objects.create(
                    professional_experience=professional_experience,
                    external_id=skill_data['id'],
                    name=skill_data['name'],
                )

        return freelancer_profile
