from rest_framework import serializers


class FreelancerSkillsSerializer(serializers.Serializer):
    id = serializers.IntegerField(source='external_id')
    name = serializers.CharField()
    duration_in_months = serializers.IntegerField(source='total_months')

    class Meta:
        fields = ['id', 'name', 'duration_in_months']
