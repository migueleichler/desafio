from rest_framework import serializers

from api.serializers.skill import SkillSerializer
from api.models import ProfessionalExperience


class ProfessionalExperienceSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    skills = SkillSerializer(many=True)

    class Meta:
        model = ProfessionalExperience
        fields = ['id', 'skills', 'company_name', 'start_date', 'end_date']
