from rest_framework import serializers

from api.models.skill import Skill


class SkillSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Skill
        fields = ['id', 'name']
