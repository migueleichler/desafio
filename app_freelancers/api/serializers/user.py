from rest_framework import serializers

from api.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = self.Meta.model.objects.create_user(**validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user
