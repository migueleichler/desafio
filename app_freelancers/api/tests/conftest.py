from typing import Dict, List

import pytest
from django.forms.models import model_to_dict
from django.db.models import Model
from model_bakery import baker
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

from api.models import Skill, User, Company, \
    ProfessionalExperience, FreelancerProfile


def get_model_fields(model: Model) -> List:
    return list(model_to_dict(model).keys())


@pytest.fixture
def skill() -> Skill:
    return baker.prepare(Skill)


@pytest.fixture
def skills() -> Skill:
    return baker.make(Skill, _quantity=5)


@pytest.fixture
def skill_dict() -> Dict:
    return {'id': 1, 'name': 'React'}


@pytest.fixture
def invalid_skill_dict() -> Dict:
    return {'names': 'React'}


@pytest.fixture
def user() -> User:
    return baker.prepare(User)


@pytest.fixture
def user_dict_with_camel_case() -> Dict:
    return {
        'username': 'python',
        'firstName': 'Guido',
        'lastName': 'van Rossum',
        'email': 'guido@mail.com',
        'password': 'figiw459ww9w',
    }


@pytest.fixture
def user_dict() -> Dict:
    return {
        'username': 'python',
        'first_name': 'Guido',
        'last_name': 'van Rossum',
        'email': 'guido@mail.com',
        'password': 'figiw459ww9w',
    }


@pytest.fixture
def company() -> Company:
    return baker.prepare(Company)


@pytest.fixture
def company_dict_with_camel_case() -> Dict:
    return {
        'username': 'psf',
        'companyName': 'Python Software Foundation',
        'password': 'psf_12345',
        'email': 'psf@mail.com',
    }


@pytest.fixture
def company_dict() -> Dict:
    return {
        'username': 'psf',
        'company_name': 'Python Software Foundation',
        'password': 'psf_12345',
        'email': 'psf@mail.com',
    }


@pytest.fixture
def api_client() -> APIClient:
    return APIClient()


def get_token(user: User) -> str:
    refresh = RefreshToken.for_user(user)

    return refresh.access_token


def get_authenticated_api_client(user: User) -> APIClient:
    token = get_token(user)

    api_client = APIClient()
    api_client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')

    return api_client


@pytest.fixture
def professional_experience() -> ProfessionalExperience:
    return baker.prepare(ProfessionalExperience)


@pytest.fixture
def freelancer_profile() -> FreelancerProfile:
    return baker.prepare(FreelancerProfile)


@pytest.fixture
def freelancer_profile_dict() -> Dict:
    return {
        "freelancer": {
            "id": 42,
            "user": {
                "first_name": "Hunter",
                "last_name": "Moore",
                "job_title": "Fullstack JS Developer",
            },
            "status": "new",
            "retribution": 650,
            "availability_date": "2018-06-13T00:00:00+01:00",
            "professional_experiences": [
                {
                    "id": 4,
                    "company_name": "Okuneva, Kerluke and Strosin",
                    "start_date": "2016-01-01T00:00:00+01:00",
                    "end_date": "2018-05-01T00:00:00+01:00",
                    "skills": [
                      {
                        "id": 241,
                        "name": "React"
                      },
                      {
                        "id": 270,
                        "name": "Node.js"
                      },
                      {
                        "id": 370,
                        "name": "Javascript"
                      }
                    ]
                },
                {
                    "id": 5,
                    "company_name": "Okuneva, Kerluke and Strosin",
                    "start_date": "2016-01-01T00:00:00+01:00",
                    "end_date": "2018-07-01T00:00:00+01:00",
                    "skills": [
                      {
                        "id": 241,
                        "name": "React"
                      }
                    ]
                }
            ]
        }
    }


@pytest.fixture
def freelancer_profile_dict_with_camel_case() -> Dict:
    return {
        "freelancer": {
            "id": 42,
            "user": {
                "firstName": "Hunter",
                "lastName": "Moore",
                "jobTitle": "Fullstack JS Developer",
            },
            "status": "new",
            "retribution": 650,
            "availabilityDate": "2018-06-13T00:00:00+01:00",
            "professionalExperiences": [
                {
                    "id": 4,
                    "companyName": "Okuneva, Kerluke and Strosin",
                    "startDate": "2016-01-01T00:00:00+01:00",
                    "endDate": "2018-05-01T00:00:00+01:00",
                    "skills": [
                      {
                        "id": 241,
                        "name": "React"
                      },
                      {
                        "id": 270,
                        "name": "Node.js"
                      },
                      {
                        "id": 370,
                        "name": "Javascript"
                      }
                    ]
                },
            ]
        }
    }


@pytest.fixture
def freelancer_profile_invalid_dict_with_camel_case() -> Dict:
    return {
        "freelancers": {
            "id": 42,
            "user": {
                "firstName": "Hunter",
                "lastName": "Moore",
                "jobTitle": "Fullstack JS Developer",
            },
            "status": "new",
            "retribution": 650,
            "availabilityDate": "2018-06-13T00:00:00+01:00",
            "professionalExperiences": [
                {
                    "id": 4,
                    "companyName": "Okuneva, Kerluke and Strosin",
                    "startDate": "2016-01-01T00:00:00+01:00",
                    "endDate": "2018-05-01T00:00:00+01:00",
                    "skills": [
                      {
                        "id": 241,
                        "name": "React"
                      },
                      {
                        "id": 270,
                        "name": "Node.js"
                      },
                      {
                        "id": 370,
                        "name": "Javascript"
                      }
                    ]
                }
            ]
        }
    }


@pytest.fixture
def professional_experience_dict() -> Dict:
    return {
        "id": 4,
        "company_name": "Okuneva, Kerluke and Strosin",
        "start_date": "2016-01-01T00:00:00+01:00",
        "end_date": "2018-05-01T00:00:00+01:00",
        "skills": [
            {
                "id": 241,
                "name": "React"
            },
            {
                "id": 270,
                "name": "Node.js"
            },
            {
                "id": 370,
                "name": "Javascript"
            }
        ]
    }
