import pytest

from api.models import Skill
from api.serializers.freelancer_profile import FreelancerProfileSerializer


class TestModelManagerSkill:
    """
    Test Case for the manager of model Skill
    """

    @pytest.mark.django_db(True)
    def test_method_get_total_experience(
        self,
        freelancer_profile_dict
    ):
        """
        Test the method created for querying the total experience of a
        freelancer in each skill in months
        """
        serializer = FreelancerProfileSerializer(data=freelancer_profile_dict)
        if serializer.is_valid():
            serializer.save()

        skills = Skill.objects.get_total_experience(
            profile_id=freelancer_profile_dict['freelancer']['id']
        )

        assert len(skills) == 3
