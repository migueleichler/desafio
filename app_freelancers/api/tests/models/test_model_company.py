import pytest

from api.tests.conftest import get_model_fields


class TestModelCompany:
    """
    Test Case for Model Company
    """

    def test_contains_fields_used_in_registration(self, company) -> None:
        """
        Test if Model contains all fields required for a New Company registration
        """
        all_fields = get_model_fields(company)

        assert 'username' in all_fields
        assert 'password' in all_fields

    @pytest.mark.django_db(True)
    def test_field_user_type_when_saved_in_database(self, company):
        """
        Test the value of field user_type that is saved in the database
        """
        company.save()

        assert company.user_type.value == "company"
