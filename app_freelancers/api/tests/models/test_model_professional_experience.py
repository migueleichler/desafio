from api.tests.conftest import get_model_fields
from api.models import ProfessionalExperience


class TestModelProfessionalExperience:
    """
    Test Case for Model ProfessionalExperience
    """

    def test_contains_all_fields_expected(
        self,
        professional_experience: ProfessionalExperience
    ) -> None:
        """
        Test if model contains all fields expected
        """
        all_fields = get_model_fields(professional_experience)

        assert all_fields == [
            'id',
            'freelancer_profile',
            'company_name',
            'start_date',
            'end_date'
        ]
