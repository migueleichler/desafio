from api.tests.conftest import get_model_fields
from api.models import Skill


class TestModelSkill:
    """
    Test Case for Model Skill
    """

    def test_contains_all_fields_expected(self, skill: Skill) -> None:
        """
        Test if model contains all fields expected
        """
        all_fields = get_model_fields(skill)

        assert all_fields == ['id', 'professional_experience', 'external_id', 'name']
