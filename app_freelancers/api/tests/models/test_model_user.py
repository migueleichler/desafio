from api.tests.conftest import get_model_fields
from api.models import User, UserType


class TestModelUser:
    """
    Test Case for Model User
    """

    def test_contains_fields_used_in_all_users_type_registration(self, user: User) -> None:
        """
        Test if model contains fields used from the base class AbstractUser

        - username
        - password
        - first_name
        - last_name
        """
        all_fields = get_model_fields(user)

        assert 'username' in all_fields
        assert 'password' in all_fields
        assert 'first_name' in all_fields
        assert 'last_name' in all_fields

    def test_field_user_type_is_enum(self, user: User) -> None:
        """
        Test if field user_type is related to Enum UserType
        """

        all_fields = get_model_fields(user)

        assert 'user_type' in all_fields
        assert type(user.user_type) == UserType

    def test_field_user_type_enum_values(self) -> None:
        """
        Test field user_type possible values
        """
        all_values = [user_type.value for user_type in UserType]

        assert len(all_values) == 1
        assert 'company' in all_values
