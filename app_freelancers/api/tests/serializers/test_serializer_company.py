from api.models.user import UserType
import pytest

from api.serializers.company import CompanySerializer


class TestCompanySerializer:
    """
    Test Case for Model Company Serializer
    """

    @pytest.mark.django_db(True)
    def test_deserialization_contains_all_fields_expected(self, company_dict) -> None:
        """
        Test data deserialization from dictionary
        """
        deserialized_data = CompanySerializer(data=company_dict)
        expected_fields = ['username', 'password', 'email']

        assert deserialized_data.is_valid(raise_exception=True)
        assert list(deserialized_data.data.keys()) == expected_fields
        assert deserialized_data.data['username'] == company_dict['username']
        assert deserialized_data.data['password'] == company_dict['password']
        assert deserialized_data.data['email'] == company_dict['email']

    @pytest.mark.django_db(True)
    def test_serialization_saving_correct_user_type_and_first_name(self, company_dict) -> None:
        """
        Test if serializer is saving correct user type and first name
        """
        serialized_data = CompanySerializer(data=company_dict)

        assert serialized_data.is_valid()

        company = serialized_data.save()

        assert company.user_type == UserType.COMPANY
