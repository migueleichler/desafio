from api.serializers.freelancer_profile import FreelancerProfileSerializer


class TestSerializerFreelancerProfile:
    """
    Test Case for Serializer of Model FreelancerProfile
    """

    def test_deserialization_contains_all_fields_expected(
        self,
        freelancer_profile_dict
    ) -> None:
        """
        Test data deserialization from dictionary
        """
        deserialized_data = FreelancerProfileSerializer(data=freelancer_profile_dict)
        expected_fields = ['id', 'professional_experiences']

        assert deserialized_data.is_valid(raise_exception=True)
        assert 'freelancer' in deserialized_data.data
        assert list(deserialized_data.data['freelancer'].keys()) == expected_fields
