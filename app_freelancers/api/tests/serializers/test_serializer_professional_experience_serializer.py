from api.serializers.professional_experience import ProfessionalExperienceSerializer


class TestProfessionalExperienceSerializer:
    """
    Test Case for Serializer of Model ProfessionalExperience
    """

    def test_deserialization_contains_all_fields_expected(
        self,
        professional_experience_dict
    ) -> None:
        """
        Test data deserialization from dictionary
        """
        deserialized_data = ProfessionalExperienceSerializer(data=professional_experience_dict)
        expected_fields = ['id', 'skills', 'company_name', 'start_date', 'end_date']

        assert deserialized_data.is_valid(raise_exception=True)
        assert list(deserialized_data.data.keys()) == expected_fields
