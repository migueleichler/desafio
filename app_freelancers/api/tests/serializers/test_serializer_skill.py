import pytest

from api.serializers.skill import SkillSerializer


class TestSkillSerializer:
    """
    Test Case for Serializer of Resource Skill
    """

    def test_deserialization_contains_all_fields_expected(self, skill_dict) -> None:
        """
        Test data deserialization from dictionary
        """
        deserialized_data = SkillSerializer(data=skill_dict)

        assert deserialized_data.is_valid()
        assert list(deserialized_data.data.keys()) == ['id', 'name']
        assert deserialized_data.data['name'] == skill_dict['name']

    @pytest.mark.django_db(True)
    def test_serialization_contains_all_fields_expected(
        self,
        freelancer_profile,
        professional_experience,
        skill
    ) -> None:
        """
        Test data serialization from dictionary
        """
        freelancer_profile.save()
        professional_experience.freelancer_profile = freelancer_profile
        professional_experience.save()
        skill.professional_experience = professional_experience
        skill.save()
        serialized_data = SkillSerializer(skill)

        assert list(serialized_data.data.keys()) == ['id', 'name']
        assert serialized_data.data['id'] == skill.id
        assert serialized_data.data['name'] == skill.name
