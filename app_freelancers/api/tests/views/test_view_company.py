import json

import pytest


class TestViewCompany:
    """
    Test Case for View of Model Freelancer
    """

    @pytest.mark.django_db(True)
    def test_valid_create_freelancer_user_request(
        self,
        api_client,
        company_dict_with_camel_case
    ):
        """
        Test a valid request for creating a new company user
        """
        response = api_client.post(
            '/api/company/',
            company_dict_with_camel_case,
            format='json'
        )
        content = json.loads(response.content)

        assert response.status_code == 201
        assert 'company' in content
        assert 'message' in content['company']
        assert content['company']['message'] == 'Usuário psf criado com sucesso'
        assert 'token' in content['company']

    @pytest.mark.django_db(True)
    def test_company_name_saved_with_success(
        self,
        api_client,
        company_dict_with_camel_case
    ):
        """
        Test if company name was saved with success
        """
        response = api_client.post(
            '/api/company/',
            company_dict_with_camel_case,
            format='json'
        )

        assert response.status_code == 201
