import json

import pytest
from rest_framework import status

from api.models import FreelancerProfile, ProfessionalExperience, Skill
from api.tests.conftest import get_authenticated_api_client


class TestViewFreelancerProfile:

    @pytest.mark.django_db(True)
    def test_valid_create_freelancer_profile_request(
        self,
        company,
        freelancer_profile_dict_with_camel_case
    ):
        """
        Test a valid request for creating a new freelancer profile
        """
        company.save()
        api_client = get_authenticated_api_client(company)
        response = api_client.post(
            '/api/freelancer/',
            freelancer_profile_dict_with_camel_case,
            format='json'
        )

        assert response.status_code == 201

    @pytest.mark.django_db(True)
    def test_create_freelancer_profile_in_database_with_success(
        self,
        company,
        freelancer_profile_dict_with_camel_case
    ):
        """
        Test if the freelancer profile is being saved in all related tables
        """
        company.save()
        api_client = get_authenticated_api_client(company)
        external_id = freelancer_profile_dict_with_camel_case['freelancer']['id']
        api_client.post(
            '/api/freelancer/',
            freelancer_profile_dict_with_camel_case,
            format='json'
        )

        freelancer_profile = FreelancerProfile.objects.filter(external_id=external_id)
        assert freelancer_profile[0].external_id == external_id
        assert freelancer_profile[0].data

        professional_experience = ProfessionalExperience.objects.filter(
            freelancer_profile=freelancer_profile[0]
        )

        assert len(professional_experience)

        skills = Skill.objects.filter(
            professional_experience=professional_experience[0]
        )

        assert len(skills)

    @pytest.mark.django_db(True)
    def test_response_content(
        self,
        company,
        freelancer_profile_dict_with_camel_case
    ):
        """
        Test the response content in a valid request
        """
        company.save()
        api_client = get_authenticated_api_client(company)
        response = api_client.post(
            '/api/freelancer/',
            freelancer_profile_dict_with_camel_case,
            format='json'
        )

        content = json.loads(response.content)

        assert response.status_code == 201
        assert 'freelancer' in content

    @pytest.mark.django_db(True)
    def test_invalid_create_freelancer_profile_request(
        self,
        company,
        freelancer_profile_invalid_dict_with_camel_case
    ):
        """
        """
        company.save()
        api_client = get_authenticated_api_client(company)
        response = api_client.post(
            '/api/freelancer/',
            freelancer_profile_invalid_dict_with_camel_case,
            format='json'
        )

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
