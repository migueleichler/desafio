import json

import pytest

from api.tests.conftest import get_authenticated_api_client


class TestViewFreelancerProfile:

    @pytest.mark.django_db(True)
    def test_valid_query_freelancer_experience_request(
        self,
        company,
        freelancer_profile_dict_with_camel_case
    ) -> None:
        """
        Test a valid request for querying a freelancer experience
        """
        company.save()
        api_client = get_authenticated_api_client(company)
        response = api_client.post(
            '/api/freelancer/',
            freelancer_profile_dict_with_camel_case,
            format='json'
        )

        assert response.status_code == 201

        response = api_client.get('/api/freelancer/42/skill/', format='json')

        assert response.status_code == 200

        content = json.loads(response.content)

        assert 'freelancer' in content
        assert 'computedSkills' in content['freelancer']
