from django.urls import path

from api.views.freelancer_profile import FreelancerProfileView
from api.views.freelancer_skills import FreelancerSkillsView
from api.views.company import CompanyView


urlpatterns = [
    path('company/', CompanyView.as_view()),
    path('freelancer/', FreelancerProfileView.as_view()),
    path('freelancer/<int:pk>/skill/', FreelancerSkillsView.as_view()),
]
