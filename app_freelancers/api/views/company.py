from api.serializers.company import CompanySerializer
from api.views.user import UserView


class CompanyView(UserView):

    serializer_class = CompanySerializer
    response_title = 'company'
