from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from api.serializers.freelancer_profile import FreelancerProfileSerializer


class FreelancerProfileView(APIView):

    def post(self, request, format=None) -> Response:
        serializer = FreelancerProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                request.data,
                status=status.HTTP_201_CREATED
            )

        return Response(
            serializer.errors,
            status=status.HTTP_422_UNPROCESSABLE_ENTITY
        )
