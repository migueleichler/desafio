from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from api.models import Skill
from api.serializers.freelancer_skills import FreelancerSkillsSerializer


class FreelancerSkillsView(APIView):

    def get(self, request, pk, format=None) -> Response:
        freelancer_profile = Skill.objects.get_total_experience(profile_id=pk)
        if freelancer_profile:
            serializer = FreelancerSkillsSerializer(freelancer_profile, many=True)
            if serializer.data:
                return Response(
                    {
                        'freelancer':
                        {
                            'id': pk,
                            'computed_skills': serializer.data,
                        }
                    },
                    status=status.HTTP_200_OK
                )
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        return Response(status=status.HTTP_404_NOT_FOUND)
