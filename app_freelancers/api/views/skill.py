from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated

from api.models import Skill
from api.serializers.skill import SkillSerializer


class ListSkillView(ListCreateAPIView):
    """
    A List View for Model Skill
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = SkillSerializer
    queryset = Skill.objects.all()
