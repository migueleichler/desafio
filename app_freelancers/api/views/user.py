from rest_framework.views import APIView
from rest_framework.response import Response
import rest_framework.status as status
from rest_framework_simplejwt.tokens import RefreshToken


class UserView(APIView):

    serializer_class = None
    response_title = None

    def post(self, request, format=None) -> Response:
        serialized_data = self.serializer_class(data=request.data)
        serialized_data.is_valid(raise_exception=True)
        user = serialized_data.save()

        token = RefreshToken.for_user(user)

        return Response(
            {
                self.response_title: {
                    'message': f'Usuário {user.username} criado com sucesso',
                    'token': str(token),
                }
            },
            status=status.HTTP_201_CREATED
        )
